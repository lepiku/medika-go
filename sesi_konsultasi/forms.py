from django import forms 
from django.db import connection
transactions=[('T{}'.format(i),'T{}'.format(i)) for i in range (1,16)]
medic_records=[('10000001','10000001'),('10000002','10000002'),('10000003','10000003')
,('10000004','10000004'),('10000005','10000005'),('10000006','10000006')
,('10000007','10000007'),('10000008','10000008'),('10000009','10000009'),
('10000010','10000010')]

class DateInput(forms.DateInput):
    input_type = 'date'

class konsulForm(forms.Form):
    no_rekam_medis=forms.ChoiceField(label='Nomor Rekam Medis Pasien:')
    tanggal=forms.DateField(label='Tanggal:',widget=DateInput(attrs={'placeholder':'mm/dd/yyyy'}))
    id_transaksi=forms.ChoiceField(label='ID Transaksi:')
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT id_transaksi FROM TRANSAKSI")
            transactions = cursor.fetchall()
            cursor.execute("SELECT no_rekam_medis FROM PASIEN")
            numbers = cursor.fetchall()
        self.fields['id_transaksi'].choices = [(r[0], r[0]) for r in transactions]
        self.fields['no_rekam_medis'].choices = [(r[0], r[0]) for r in numbers]



class konsulUpdateForm(forms.Form):
    id_konsultasi=forms.CharField(max_length=50)
    no_rekam_medis_pasien=forms.CharField(label='Nomor Rekam Medis Pasien:', max_length=50)
    tanggal=forms.DateField(label='Tanggal:',widget=DateInput(attrs={'placeholder':'mm/dd/yyyy'}))
    biaya=forms.IntegerField(label='Biaya:')
    status=forms.CharField(label='Status:',max_length=50,widget=forms.TextInput(attrs={'placeholder':'Isi status dengan Booked atau Done'}))
    id_transaksi=forms.CharField(label='ID Transaksi:')
from django.urls import include,path
from .views import form_page,update,read,delete
app_name='sesi_konsultasi'
urlpatterns =[
    path('create/',form_page,name='konsul_form'),
    path('update/<str:id>/',update,name='konsul_form_update'),
    path('read/',read,name='konsul_form_read'),
    path('delete/<str:id>/',delete,name='konsul_delete')
]
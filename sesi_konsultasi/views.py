from django.shortcuts import render,redirect
from .forms import konsulForm,konsulUpdateForm
from django.urls import reverse
from django.db import connection,IntegrityError
import re
from django.http import HttpResponse,JsonResponse
from tindakan.views import dictfetchall,dictfetchone
from pengguna.utils import role_required
import json


def generate_id():
    latest_id=list()
    with connection.cursor() as cursor1:
        cursor1.execute("SELECT id_konsultasi  FROM SESI_KONSULTASI")
        latest_id=[item[0] for item in cursor1.fetchall()]
    if(len(latest_id) == 0):
        return 'K0'
    latest_id=[int(id[1:]) for id in latest_id]
    latest_id.sort(reverse=True)
    return 'K'+str(latest_id[0]+1)

#Page for create feature
@role_required("admin")
def form_page(request):
    form=konsulForm()
    if(request.method=="POST"):
        form=konsulForm(request.POST)
        if(form.is_valid()):
            id_konsultasi=generate_id()
            no_rekam_medis_pasien=form.cleaned_data['no_rekam_medis']
            tanggal=form.cleaned_data['tanggal']
            status='Booked'
            biaya='0'
            id_transaksi=form.cleaned_data['id_transaksi']
            try:
                with connection.cursor() as cursor1:
                    cursor1.execute("INSERT INTO SESI_KONSULTASI VALUES (%s,%s,%s,%s,%s,%s)",[id_konsultasi,no_rekam_medis_pasien,tanggal,biaya,status,id_transaksi])
                return redirect('sesi_konsultasi:konsul_form_read')
            except IntegrityError:
                return HttpResponse("ERROR: Transaction ID cant be duplicate!")
    context={
            'form': form,
            'title': 'Form Reservasi Sesi Konsultasi',
            'target_url' : reverse('sesi_konsultasi:konsul_form')
        }
    return render(request,'tindakan/generic_form.html',context)

#Page for update 
@role_required("admin")
def update(request,id):
        selected_konsul=''
        with connection.cursor() as cursor1:
            cursor1.execute("SELECT * FROM SESI_KONSULTASI WHERE id_konsultasi = %s",[id])
            selected_konsul = cursor1.fetchone()

        if(request.method=="POST"):
            form=konsulUpdateForm(request.POST)
            form.fields['id_konsultasi'].required=False
            form.fields['id_transaksi'].required=False
            form.fields['biaya'].required=False
            form.fields['no_rekam_medis_pasien'].required=False
            if(form.is_valid()):
                status=form.cleaned_data['status']
                tanggal=form.cleaned_data['tanggal']
                with connection.cursor() as cursor1:
                    cursor1.execute("UPDATE SESI_KONSULTASI SET status=%s, tanggal=%s WHERE id_konsultasi=%s",[status,tanggal,id])
                return redirect('sesi_konsultasi:konsul_form_read')
            return HttpResponse(status=404)

        else:
            form=konsulUpdateForm()
            i=0
            for field in form.fields:
                form[field].initial=selected_konsul[i]
                if field!='tanggal' and field!='status':
                    form.fields[field].required=False
                    form.fields[field].disabled=True
                i+=1
            context={
                'form': form,
                'title': 'Form Update Sesi Konsultasi',
                'target_url' : reverse('sesi_konsultasi:konsul_form_update',kwargs={'id':id}),
                'submit_text':'Update'
            }
            return render(request,'tindakan/generic_form.html',context)

#Delete data
@role_required("admin")
def delete(request,id):
    try:
        with connection.cursor() as cursor1:
            cursor1.execute("DELETE FROM SESI_KONSULTASI WHERE SESI_KONSULTASI.id_konsultasi= %s",[id])
        return redirect('sesi_konsultasi:konsul_form_read')
    except IntegrityError:
        return HttpResponse('ERROR: Primary key of this row is still being referenced!')

def shorter(selected,all):
    return len(selected) < len(all)

#Read table
def read(request):
    limit=5
    if(request.session.is_empty()):
        return redirect("pengguna:profile")
    elif(request.method=='POST'):
        mode=request.POST.get("mode","forward");
        if(mode=="forward"):
            offset=limit*int(request.POST.get("offset",0))
        else:
            offset=limit*(int(request.POST.get("offset",0))-2)
        data=dict()
        with connection.cursor() as cursor2:
            if(request.session['role']=='dokter'):
                cursor2.execute("SELECT * FROM SESI_KONSULTASI WHERE id_konsultasi IN (SELECT id_konsultasi FROM DAFTAR_TINDAKAN WHERE id_tindakan_poli IN (SELECT id_tindakan_poli FROM \
                TINDAKAN_POLI where id_poliklinik IN (SELECT id_poliklinik FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_dokter=%s))) ORDER BY tanggal DESC OFFSET %s LIMIT %s",[request.session['id_dokter'],offset,limit])   
            elif(request.session['role']=='pasien'):
                cursor2.execute("SELECT * FROM SESI_KONSULTASI WHERE no_rekam_medis_pasien=%s ORDER BY tanggal DESC OFFSET %s LIMIT %s",[offset,request.session['no_rekam_medis'],limit])
            else:
                cursor2.execute("SELECT * FROM SESI_KONSULTASI ORDER BY id_konsultasi ASC OFFSET %s LIMIT %s",[offset,limit])
            data=dictfetchall(cursor2)
        for i, d in enumerate(data):
            data[i]['pk'] = d['id_konsultasi']
        return JsonResponse(data,safe=False)

    else:
        header = [
            'ID Konsultasi',
            'Nomor Pasien',
            'Tanggal',
            'Biaya',
            'Status',
            'ID Transaksi',
        ]
        data=[]
        selected_data=[]
        with connection.cursor() as cursor:
            if(request.session['role']=='dokter'):
                cursor.execute("SELECT * FROM SESI_KONSULTASI WHERE id_konsultasi IN (SELECT id_konsultasi FROM DAFTAR_TINDAKAN WHERE id_tindakan_poli IN (SELECT id_tindakan_poli FROM \
                TINDAKAN_POLI where id_poliklinik IN (SELECT id_poliklinik FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_dokter=%s))) ORDER BY tanggal DESC",[request.session['id_dokter']])   
            elif(request.session['role']=='pasien'):
                cursor.execute("SELECT * FROM SESI_KONSULTASI WHERE no_rekam_medis_pasien=%s ORDER BY tanggal DESC",[request.session['no_rekam_medis']])
            else:
                cursor.execute("SELECT * FROM SESI_KONSULTASI ORDER BY id_konsultasi ASC")
            data = dictfetchall(cursor)

        with connection.cursor() as cursor2:
            if(request.session['role']=='dokter'):
                cursor2.execute("SELECT * FROM SESI_KONSULTASI WHERE id_konsultasi IN (SELECT id_konsultasi FROM DAFTAR_TINDAKAN WHERE id_tindakan_poli IN (SELECT id_tindakan_poli FROM \
                TINDAKAN_POLI where id_poliklinik IN (SELECT id_poliklinik FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_dokter=%s))) ORDER BY tanggal DESC LIMIT %s",[request.session['id_dokter'],limit])   
            elif(request.session['role']=='pasien'):
                cursor2.execute("SELECT * FROM SESI_KONSULTASI WHERE no_rekam_medis_pasien=%s ORDER BY tanggal DESC LIMIT %s",[request.session['no_rekam_medis'],limit])
            else:
                cursor2.execute("SELECT * FROM SESI_KONSULTASI ORDER BY id_konsultasi ASC LIMIT %s",[limit])
            selected_data=dictfetchall(cursor2)
        pagination=False
        if(shorter(selected_data,data)):
            data=selected_data
            pagination=True
        if(data !=None):
            for i, d in enumerate(data):
                data[i]['pk'] = d['id_konsultasi']
                data[i]['tanggal'] = d['tanggal'].strftime('%Y-%m-%d')
            context = {
                'title': 'Daftar Sesi Konsultasi',
                'header': header,
                'data': data,
                'update_url': '/sesi_konsultasi/update/',
                'delete_url': '/sesi_konsultasi/delete/',
                'api_url':'/sesi_konsultasi/read/',
                'admin_links': [
                {'name': 'create', 'url' : reverse('sesi_konsultasi:konsul_form')},
                
            ],
            'pagination':pagination
        }

        else:
            context = {
                'title': 'Daftar Sesi Konsultasi',
                'admin_links': [
                {'name': 'create', 'url' : reverse('sesi_konsultasi:konsul_form')}
            ]
        }
        return render(request, 'tindakan/generic_read.html', context)




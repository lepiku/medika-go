from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'
    
class adminForm(forms.Form):
    username=forms.CharField(label='Username:', max_length=50,widget=forms.TextInput(attrs={'placeholder':'Isi username Anda di sini (maks 50 karakter)'}))
    password=forms.CharField(label='Password:',max_length=128,widget=forms.PasswordInput(attrs={'placeholder':'Isi pass Anda di sini (maks 128 karakter)'}))
    no_identitas=forms.CharField(label='Nomor identitas:',max_length=50,widget=forms.TextInput(attrs={'placeholder':'Isi no identitas Anda(KTP, Kartu pelajar,dsb) di sini (maks 50 karakter)'}))
    nama_lengkap=forms.CharField(label='Nama lengkap:',max_length=50,widget=forms.TextInput(attrs={'placeholder':'Isi nama lengkap Anda di sini (maks 50 karakter)'}))
    tanggal_lahir=forms.DateField(label='Tanggal lahir:',widget=DateInput(attrs={'placeholder':'mm/dd/yyyy'}))
    email=forms.CharField(label='Email:', max_length=50,widget=forms.TextInput(attrs={'placeholder':'Isi email Anda di sini (maks 50 karakter dengan format <akun>@<domain email>)'}))
    alamat=forms.CharField(label='Alamat:',widget=forms.Textarea(attrs={'placeholder':'Isi alamat rumah/kost/kontrakan Anda di sini'}))
from django.shortcuts import render,reverse,redirect
from .forms import adminForm
from django.db import connection
from pengguna.views import auth_login
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
import random as r 

def assign_rs():
    lst=[]
    with connection.cursor() as cursor:
        cursor.execute("SELECT kode_rs FROM RS_CABANG")
        data=cursor.fetchall()
        for i in range(len(data)):
            lst.append(data[i][0])
    return r.choice(lst)

def validate_email(email):
    result=None
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM PENGGUNA WHERE email=%s",[email])
        result=cursor.fetchone()
    return result == None

def validate_username(username):
    result=None
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM PENGGUNA WHERE username=%s",[username])
        result=cursor.fetchone()
    return result == None

def validate_id(nomor_id):
    result=None
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM PENGGUNA WHERE nomor_id=%s",[nomor_id])
        result=cursor.fetchone()
    return result == None

def generate_np():
    value=''
    with connection.cursor() as cursor:
        cursor.execute("SELECT nomor_pegawai FROM ADMINISTRATOR ORDER BY nomor_pegawai DESC LIMIT 1")
        value=str(int(cursor.fetchone()[0])+1)
    if(value == None):
        return 0
    return value

def form_admin(request):
    if(request.session.is_empty()):
        form=adminForm()
        if (request.method=='POST'):
            form=adminForm(request.POST)
            if(form.is_valid()):
                username=form.cleaned_data['username']
                password=form.cleaned_data['password']
                nomor_id=form.cleaned_data['no_identitas']
                nama_lengkap=form.cleaned_data['nama_lengkap']
                tanggal_lahir=form.cleaned_data['tanggal_lahir']
                email=form.cleaned_data['email']
                alamat=form.cleaned_data['alamat']
                if (not validate_email(email)) or (not validate_username(username)) or (not validate_id(nomor_id)):
                    if( not validate_email(email)):
                        form.add_error('email','Email cant be duplicate!')
                    if( not validate_username(username)):
                        form.add_error('username','Username is already exist!')
                    if( not validate_id(nomor_id)):
                        form.add_error('no_identitas','Id number is already exist!')
                    context = {'form':form}
                    return render(request,'generic_regist.html',{'form':form})
                with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO PENGGUNA VALUES(%s, %s, %s, %s, %s,%s,%s)", [email, username, password, nama_lengkap, nomor_id, tanggal_lahir,alamat])
                    no_pegawai=generate_np()
                    kode_rs=assign_rs()
                    cursor.execute("INSERT INTO ADMINISTRATOR VALUES (%s,%s,%s)",[no_pegawai,username,kode_rs])
                    auth_login(request,username,password)
                    request.session['success_registered']=True
                    return redirect('pengguna:redirect')
        context={
            'form':form,
            'title':"Form Registrasi Administrator",
            'target_url': reverse('admin_rs:form_admin')
        }
        return render(request,'generic_regist.html',context)
    else:
        return HttpResponse(status=404)

def homepage(request):
    if(request.session.is_empty()):
        return redirect("pengguna:profile")
    else:
        if(request.session['role']=='admin'):
            context={
                        'username':request.session['username'],
                        'nomor_id':request.session['nomor_id'],
                        'nama_lengkap':request.session['nama_lengkap'],
                        'tanggal_lahir':request.session['tanggal_lahir'],
                        'alamat':request.session['alamat'],
                        'email':request.session['email']
                    }
            return render(request,'profile_admin.html',context)
        else:
            raise PermissionDenied

from django.urls import include,path
from .views import form_admin,homepage
app_name='admin_rs'
urlpatterns =[
    path('register/',form_admin,name='form_admin'),
    path('profile/',homepage,name='admin_profile')
    ]
from django.contrib import messages
from django.db import connection
from django.db.utils import IntegrityError
from django.shortcuts import render, redirect
from django.urls import reverse

from pengguna.utils import role_required
from tindakan.views import dictfetchall, dictfetchone

from .forms import (CreateLayananPoliklinik, UpdateLayananPoliklinikForm,
                    UpdateJadwalPoliklinikForm)


@role_required('admin')
def createl(request):
    if request.method == 'POST':
        form = CreateLayananPoliklinik(request.POST)
        if form.is_valid():
            form.save()
            return redirect('layanan_poli:layanan_read')
    else:
        form = CreateLayananPoliklinik()

    context = {
        'title': 'Form pembuatan Layanan Poliklinik',
        'form': form,
    }

    return render(request, 'create_layanan_poliklinik.html', context)


@role_required('admin')
def updatel(request, id):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT * FROM layanan_poliklinik WHERE id_poliklinik = %s",
            [id,],
        )
        poli = dictfetchone(cursor)

    if request.method == 'POST':
        form = UpdateLayananPoliklinikForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('layanan_poli:layanan_read')
    else:
        form = UpdateLayananPoliklinikForm(poli=poli)

    context = {
        'form': form,
        'title': 'Form Update Layanan Poliklinik',
        'submit_text': 'Update',
    }

    return render(request, 'tindakan/generic_form.html', context)


@role_required('admin')
def deletel(request, id):
    with connection.cursor() as cursor:
        try:
            cursor.execute(
                "DELETE FROM layanan_poliklinik WHERE id_poliklinik = %s",
                (id,))
        except IntegrityError:
            messages.error(request, "Layanan {} masih disebutkan di jadwal".format(id))
    return redirect('layanan_poli:layanan_read')


def readl(request):
    header = (
        'ID Layanan Poliklinik',
        'Nama',
        'Deskripsi',
        'Kode RS',
        'Jadwal',
    )

    with connection.cursor() as cursor:
        cursor.execute('''SELECT id_poliklinik, nama, deskripsi, kode_rs_cabang
                          FROM layanan_poliklinik ORDER BY id_poliklinik''')
        data = dictfetchall(cursor)

    for i, d in enumerate(data):
        data[i]['pk'] = d['id_poliklinik']

    context={
        'title': 'Daftar Layanan Poliklinik',
        'header': header,
        'data': data,
        'jadwal_url': '/layanan_poli/read_jadwal/',
        'update_url': '/layanan_poli/update_layanan/',
        'delete_url': '/layanan_poli/delete_layanan/',
        'admin_links': [
            {'name': 'create', 'url': reverse('layanan_poli:layanan_create')},
        ],
    }

    return render(request, 'read_layanan_poli.html', context)


@role_required('admin')
def updatej(request, id):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT * FROM jadwal_layanan_poliklinik WHERE id_jadwal_poliklinik = %s",
            (id,),
        )
        jadwal = dictfetchone(cursor)

    if request.method == 'POST':
        form = UpdateJadwalPoliklinikForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('layanan_poli:jadwal_read')
    else:
        form = UpdateJadwalPoliklinikForm(jadwal=jadwal)

    context = {
        'form': form,
        'title': 'Form Update Jadwal Layanan Poliklinik',
        'submit_text': 'Update',
    }

    return render(request, 'tindakan/generic_form.html', context)


@role_required('admin')
def deletej(request, id):
    with connection.cursor() as cursor:
        cursor.execute(
            "DELETE FROM jadwal_layanan_poliklinik WHERE id_jadwal_poliklinik = %s",
            (id,))
    return redirect('layanan_poli:jadwal_read')


def readj(request):
    header = (
        'ID Jadwal Poliklinik',
        'Hari',
        'Waktu Mulai',
        'Waktu Selesai',
        'Kapasitas',
        'ID Dokter',
        'ID Poliklinik',
    )

    with connection.cursor() as cursor:
        cursor.execute('''SELECT id_jadwal_poliklinik, hari, waktu_mulai,
                          waktu_selesai, kapasitas, id_dokter, id_poliklinik
                          FROM jadwal_layanan_poliklinik
                          ORDER BY id_jadwal_poliklinik''')
        data = dictfetchall(cursor)

    for i, d in enumerate(data):
        data[i]['pk'] = d['id_jadwal_poliklinik']

    context={
        'title': 'Daftar Jadwal Poliklinik',
        'header': header,
        'data': data,
        'update_url' : '/layanan_poli/update_jadwal/',
        'delete_url' : '/layanan_poli/delete_jadwal/',
    }

    return render(request,'tindakan/generic_read.html',context)

from django.urls import path
from .views import (createl, updatel, readl, deletel, updatej, readj, deletej)

app_name = 'layanan_poli'

urlpatterns = [
    path('create_layanan/', createl, name='layanan_create'),
    path('update_layanan/<str:id>/', updatel, name='layanan_update'),
    path('read_layanan/', readl, name='layanan_read'),
    path('delete_layanan/<str:id>/', deletel, name='layanan_delete'),

    path('update_jadwal/<str:id>/', updatej, name='jadwal_update'),
    path('read_jadwal/', readj, name='jadwal_read'),
    path('delete_jadwal/<str:id>/', deletej, name='jadwal_delete')
]

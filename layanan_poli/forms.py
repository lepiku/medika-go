from django import forms
from django.db import connection


def create_jadwal(hari, jam_mulai, jam_selesai, kapasitas, dokter, poli):
    with connection.cursor() as cursor:
        cursor.execute('''SELECT id_jadwal_poliklinik FROM jadwal_layanan_poliklinik
                          ORDER BY substr(id_jadwal_poliklinik, 4)::INTEGER DESC LIMIT 1''')
        try:
            prev_id = cursor.fetchone()[0]
        except IndexError:
            prev_id = 'LPO0'
        next_id = prev_id[:3] + str(int(prev_id[3:]) + 1)

        cursor.execute(
            'INSERT INTO jadwal_layanan_poliklinik VALUES(%s, %s, %s, %s, %s, %s, %s)',
            (next_id, jam_mulai, jam_selesai, hari, kapasitas, dokter, poli),
        )


class CreateLayananPoliklinik(forms.Form):
    nama_layanan = forms.CharField(label='Nama Layanan', max_length=50)
    deskripsi = forms.CharField(label='Deskripsi', max_length=255)
    kode_rs = forms.ChoiceField(label='Kode RS')
    daftar_jadwal = forms.MultipleChoiceField(label='Daftar Jadwal', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        with connection.cursor() as cursor:
            cursor.execute("SELECT kode_rs FROM rs_cabang")
            rows = cursor.fetchall()

        self.fields['kode_rs'].choices = [(r[0], r[0]) for r in rows]
        if len(args) >= 1:
            self.fields['daftar_jadwal'].choices = [
                (x, x) for x in args[0].getlist('daftar_jadwal')
            ]

    def save(self):
        with connection.cursor() as cursor:
            cursor.execute('''SELECT id_poliklinik FROM layanan_poliklinik
                              ORDER BY substr(id_poliklinik, 4)::INTEGER DESC LIMIT 1''')
            try:
                prev_id = cursor.fetchone()[0]
            except IndexError:
                prev_id = 'LPO0'
            next_id = prev_id[:3] + str(int(prev_id[3:]) + 1)

            cursor.execute(
                'INSERT INTO layanan_poliklinik VALUES(%s, %s, %s, %s)',
                (
                    next_id,
                    self.cleaned_data['kode_rs'],
                    self.cleaned_data['nama_layanan'],
                    self.cleaned_data['deskripsi'],
                ))

            cursor.execute('SELECT id_dokter FROM dokter LIMIT 1')
            id_dokter = cursor.fetchone()[0]

            for jadwal in self.cleaned_data['daftar_jadwal']:
                jadwal = jadwal.split(';')
                jam = jadwal[1].split('-')
                create_jadwal(
                    jadwal[0].strip(),
                    jam[0].strip(),
                    jam[1].strip(),
                    jadwal[2].strip(),
                    id_dokter,
                    next_id)


class UpdateLayananPoliklinikForm(forms.Form):
    id_poliklinik=forms.CharField(label='ID Poliklinik:',
                                  widget=forms.TextInput({'readonly': True}))
    nama=forms.CharField(label='Nama Layanan:',max_length=50)
    deskripsi=forms.CharField(label='Deskripsi:',max_length=150)
    rspl=forms.ChoiceField(label='Rumah Sakit Penyedia Layanan:')

    def __init__(self, *args, poli=None, **kwargs):
        super().__init__(*args, **kwargs)

        with connection.cursor() as cursor:
            cursor.execute("SELECT kode_rs FROM rs_cabang")
            rows = cursor.fetchall()

        self.fields['rspl'].choices = [(r[0], r[0]) for r in rows]

        if poli is not None:
            self.fields['rspl'].initial = poli['kode_rs_cabang']
            self.fields['nama'].initial = poli['nama']
            self.fields['deskripsi'].initial = poli['deskripsi']
            self.fields['id_poliklinik'].initial = poli['id_poliklinik']

    def save(self):
        with connection.cursor() as cursor:
            cursor.execute('''UPDATE layanan_poliklinik SET
                              kode_rs_cabang = %s, nama = %s, deskripsi = %s
                              WHERE id_poliklinik = %s''',
                (
                    self.cleaned_data['rspl'],
                    self.cleaned_data['nama'],
                    self.cleaned_data['deskripsi'],
                    self.cleaned_data['id_poliklinik'],
                ))

class UpdateJadwalPoliklinikForm(forms.Form):
    id_jadwal_poli=forms.CharField(label='ID Jadwal Layanan Poliklinik:',
                                   widget=forms.TextInput({'readonly': True}))
    hari=forms.CharField(label='Hari:',max_length=50)
    waktu_mulai=forms.TimeField(label='Waktu Mulai:')
    waktu_selesai=forms.TimeField(label='Waktu Selesai:')
    kapasitas=forms.IntegerField(label='Kapasitas:',initial=1)
    id_dokter=forms.ChoiceField(label='ID Dokter:')
    id_poliklinik=forms.CharField(label='Rumah Sakit Penyedia Layanan:',
                                  widget=forms.TextInput({'readonly': True}))

    def __init__(self, *args, jadwal=None, **kwargs):
        super().__init__(*args, **kwargs)

        with connection.cursor() as cursor:
            cursor.execute("SELECT id_dokter FROM dokter")
            rows = cursor.fetchall()

        self.fields['id_dokter'].choices = [(r[0], r[0]) for r in rows]

        if jadwal is not None:
            self.fields['id_jadwal_poli'].initial = jadwal['id_jadwal_poliklinik']
            self.fields['hari'].initial = jadwal['hari']
            self.fields['waktu_mulai'].initial = jadwal['waktu_mulai']
            self.fields['waktu_selesai'].initial = jadwal['waktu_selesai']
            self.fields['kapasitas'].initial = jadwal['kapasitas']
            self.fields['id_dokter'].initial = jadwal['id_dokter']
            self.fields['id_poliklinik'].initial = jadwal['id_poliklinik']

    def save(self):
        with connection.cursor() as cursor:
            cursor.execute('''UPDATE jadwal_layanan_poliklinik
                              SET hari = %s, waktu_mulai = %s, waktu_selesai = %s,
                              kapasitas = %s, id_dokter = %s
                              WHERE id_poliklinik = %s''',
                (
                    self.cleaned_data['hari'],
                    self.cleaned_data['waktu_mulai'],
                    self.cleaned_data['waktu_selesai'],
                    self.cleaned_data['kapasitas'],
                    self.cleaned_data['id_dokter'],
                    self.cleaned_data['id_poliklinik'],
                ))

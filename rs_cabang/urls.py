from django.urls import include,path
from .views import form_page,update,read,delete
app_name='rs_cabang'
urlpatterns =[
    path('create/',form_page,name='rs_form'),
    path('update/<str:id>/', update, name='rs_form_update'),
    path('delete/<str:id>/', delete, name='rs_delete'),
    path('read/',read,name='rs_form_read')
    ]
from django import forms
from functools import partial

class DateInput(forms.DateInput):
    input_type = 'date'

class rsForm(forms.Form):
    kode_rs=forms.CharField(max_length=50,label='Kode RS Cabang:',widget=forms.TextInput(attrs={'placeholder': 'Isi kode rs dengan format RS<angka> '}))
    nama=forms.CharField(max_length=50,label='Nama:',widget=forms.TextInput(attrs={'placeholder': 'Isi nama rumah sakit di sini (maksimal 50 karakter)'}))
    tanggal_pendirian=forms.DateField(label='Tanggal Pendirian:',widget=DateInput(attrs={'placeholder':'mm/dd/yyyy'}))
    jalan=forms.CharField(label='Jalan:',widget=forms.Textarea(attrs={'placeholder': 'Isi posisi jalan rumah sakit di sini '}))
    nomor=forms.IntegerField(label='Nomor:',widget=forms.TextInput(attrs={'placeholder': 'Isi nomor dari alamat rumah sakit di sini'}))
    kota=forms.CharField(label='Kota:',widget=forms.TextInput(attrs={'placeholder': 'Isi kota domisli rumah sakit di sini'}))

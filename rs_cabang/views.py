from django.shortcuts import render,redirect
from .forms import rsForm
from django.db import connection, IntegrityError
from django.urls import reverse
from django.http import HttpResponse,JsonResponse
from tindakan.views import dictfetchall,dictfetchone
from pengguna.utils import role_required

@role_required("admin")
def form_page(request):
    form=rsForm()
    if(request.method=="POST"):
        form=rsForm(request.POST)
        if(form.is_valid()):
            kode_rs=form.cleaned_data['kode_rs']
            nama=form.cleaned_data['nama']
            tanggal_pendirian=form.cleaned_data['tanggal_pendirian']
            jalan=form.cleaned_data['jalan']
            nomor=form.cleaned_data['nomor']
            kota=form.cleaned_data['kota']
            try:
                with connection.cursor() as cursor1:
                    cursor1.execute("INSERT INTO RS_CABANG VALUES (%s,%s,%s,%s,%s,%s)",[kode_rs,nama,tanggal_pendirian,jalan,nomor,kota])
                return redirect('rs_cabang:rs_form_read')
            except IntegrityError:
                form.add_error('kode_rs','Maaf kode RS yang dimasukkan sudah tersedia')
                context={
                    'form': form,
                    'title': 'Form Registrasi RS Cabang',
                    'target_url' : reverse('rs_cabang:rs_form')
                }     
                return render(request,'tindakan/generic_form.html',context)

    context={
            'form': form,
            'title': 'Form Registrasi RS Cabang',
            'target_url' : reverse('rs_cabang:rs_form')
        }     
    return render(request,'tindakan/generic_form.html',context)

@role_required("admin")
def delete(request,id):
    try:
        with connection.cursor() as cursor1:
            cursor1.execute("DELETE FROM RS_CABANG WHERE RS_CABANG.kode_rs= %s",[id])
        return redirect('rs_cabang:rs_form_read')
    except IntegrityError:
        return HttpResponse("ERROR: Primary key is still being referenced!")

@role_required("admin")
def update(request,id):
    selected_rs=''
    with connection.cursor() as cursor1:
            cursor1.execute("SELECT * FROM RS_CABANG WHERE kode_rs = %s",[id])
            selected_rs = cursor1.fetchone()
    if(request.method=='POST'):
        #TODO : Add form input handling in TK5
        form=rsForm(request.POST)
        form.fields['kode_rs'].required=False
        form.fields['kode_rs'].disabled=True
        if(form.is_valid()):
            nama=form.cleaned_data['nama']
            tanggal_pendirian=form.cleaned_data['tanggal_pendirian']
            jalan=form.cleaned_data['jalan']
            nomor=form.cleaned_data['nomor']
            kota=form.cleaned_data['kota']
            with connection.cursor() as cursor1:
                cursor1.execute("UPDATE RS_CABANG SET nama=%s, tanggal_pendirian=%s, jalan=%s,nomor=%s,kota=%s WHERE kode_rs=%s",[nama,tanggal_pendirian,jalan,nomor,kota,id])
            return redirect('rs_cabang:rs_form_read')
        return HttpResponse(status=404)
    else:
        form=rsForm()
        i=0
        for field in form.fields:
            form[field].initial=selected_rs[i]
            i+=1
        form.fields['kode_rs'].required=False
        form.fields['kode_rs'].disabled=True
        context={
            'form': form,
            'title': 'Form Update RS Cabang',
            'target_url' : reverse('rs_cabang:rs_form_update', kwargs={'id': id}),
            'submit_text':'Update'
        }
        return render(request,'tindakan/generic_form.html',context)

def read(request):
    limit=5
    if(request.session.is_empty()):
        return redirect("pengguna:profile")
    elif (request.method=="POST"):
        mode=request.POST.get("mode","forward")
        if(mode=="forward"):
            offset=limit*int(request.POST.get("offset",0))
        else:
            offset=limit*(int(request.POST.get("offset",0))-2)
        data=dict()
        with connection.cursor() as cursor2:
            cursor2.execute("SELECT * FROM RS_CABANG ORDER BY kode_rs ASC OFFSET %s LIMIT %s",[offset,limit])
            data=dictfetchall(cursor2)
        for i, d in enumerate(data):
            data[i]['pk'] = d['kode_rs']
            data[i]['tanggal_pendirian']=d['tanggal_pendirian'].strftime('%Y-%m-%d')
        return JsonResponse(data,safe=False)
    else:
        header = [
            'Kode RS',
            'Nama',
            'Tanggal Pendirian',
            'Jalan',
            'Kota',
            'Nomor',
        ]
        data=[]
        selected_data=[]
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM RS_CABANG ORDER BY kode_rs ASC")
            data = dictfetchall(cursor)
        
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM RS_CABANG ORDER BY kode_rs ASC LIMIT %s",[limit])
            selected_data = dictfetchall(cursor)

        if(len(selected_data) < len(data)):
            data=selected_data
            pagination=True

        for i, d in enumerate(data):
            data[i]['pk'] = d['kode_rs']
            data[i]
        context = {
            'title': 'Daftar RS Cabang',
            'header': header,
            'data': data,
            'update_url': '/rs_cabang/update/',
            'delete_url': '/rs_cabang/delete/',
            'admin_links': [
            {'name': 'create', 'url' : reverse('rs_cabang:rs_form')}
            ],
            'pagination':pagination
        }
        return render(request, 'tindakan/generic_read.html', context)




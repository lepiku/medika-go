from django.shortcuts import render,redirect
from .forms import transaksiForm,transaksiUpdateForm
from django.urls import reverse
from datetime import datetime,date
from django.db import connection,IntegrityError
from django.http import HttpResponse
from tindakan.views import dictfetchall,dictfetchone
from pengguna.utils import role_required
# Create your views here.


def generate_id():
    latest_id=list()
    with connection.cursor() as cursor1:
        cursor1.execute("SELECT id_transaksi  FROM TRANSAKSI")
        latest_id=[item[0] for item in cursor1.fetchall()]
    if(len(latest_id) == 0):
        return 'T0'
    latest_id=[int(id[1:]) for id in latest_id]
    latest_id.sort(reverse=True)
    return 'T'+str(latest_id[0]+1)

def generate_date():
    return date.today()

def generate_time():
    return datetime.now()

@role_required("admin")
def form(request):
    form=transaksiForm()
    if(request.method=="POST"):
        form=transaksiForm(request.POST)
        if(form.is_valid()):
            id_transaksi=generate_id()
            tanggal=generate_date()
            status='Created'
            total_biaya='0'
            waktu_pembiayaan=generate_time()
            no_rekam_medis=form.cleaned_data['no_rekam_medis']
            try:
                with connection.cursor() as cursor1:
                    cursor1.execute("INSERT INTO TRANSAKSI VALUES (%s,%s,%s,%s,%s,%s)",[id_transaksi,tanggal,status,total_biaya,waktu_pembiayaan,no_rekam_medis])
                return redirect('transaksi:transaksi_read')
            except IntegrityError:
                return HttpResponse("ERROR: Transaction ID cant be duplicate!")
    context={
            'form': form,
            'title': 'Form Transaksi',
            'target_url' : reverse('transaksi:transaksi_form')
        }
    return render(request,'tindakan/generic_form.html',context)

@role_required("admin")
def update(request,id):
        selected_transaksi=''
        with connection.cursor() as cursor1:
            cursor1.execute("SELECT * FROM TRANSAKSI WHERE id_transaksi = %s",[id])
            selected_transaksi = cursor1.fetchone()

        if(request.method=="POST"):
            form=transaksiUpdateForm(request.POST)
            form.fields['id_transaksi'].required=False
            form.fields['total_biaya'].required=False
            form.fields['no_rekam_medis_pasien'].required=False
            if(form.is_valid()):
                status=form.cleaned_data['status']
                tanggal=form.cleaned_data['tanggal']
                waktu_pembiayaan=form.cleaned_data['waktu_pembiayaan'].replace(tzinfo=None)
                with connection.cursor() as cursor1:
                    cursor1.execute("UPDATE TRANSAKSI SET status=%s, tanggal=%s, waktu_pembiayaan=%s WHERE id_transaksi=%s",[status,tanggal,waktu_pembiayaan,id])
                return redirect('transaksi:transaksi_read')
            return HttpResponse(status=404)

        else:
            form=transaksiUpdateForm()
            i=0
            for field in form.fields:
                form[field].initial=selected_transaksi[i]
                if field!='tanggal' and field!='status' and field!='waktu_pembiayaan':
                    form.fields[field].required=False
                    form.fields[field].disabled=True
                i+=1
            context={
                'form': form,
                'title': 'Form Update Transaksi',
                'target_url' : reverse('transaksi:transaksi_update',kwargs={'id':id}),
                'submit_text':'Update'
            }
            return render(request,'tindakan/generic_form.html',context)

@role_required("admin")
def delete(request,id):
    try:
        with connection.cursor() as cursor1:
            cursor1.execute("DELETE FROM TRANSAKSI WHERE TRANSAKSI.id_transaksi= %s",[id])
        return redirect('transaksi:transaksi_read')
    except IntegrityError:
        return HttpResponse('ERROR: Primary key of this row is still being referenced!')


def read(request):
    if(request.session.is_empty()):
        return redirect("pengguna:profile")
    else:
        header = [
            'ID Transaksi',
            'Tanggal',
            'Status',
            'Total Biaya',
            'Waktu Pembiayaan',
            'No Rekam Medis',
        ]
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM TRANSAKSI ORDER BY id_transaksi ASC")
            data = dictfetchall(cursor)
        if(data !=None):
            for i, d in enumerate(data):
                data[i]['pk'] = d['id_transaksi']
            context = {
                'title': 'Daftar Sesi Transaksi',
                'header': header,
                'data': data,
                'update_url': '/transaksi/update/',
                'delete_url': '/transaksi/delete/',
                'admin_links': [
                {'name': 'create', 'url' : reverse('transaksi:transaksi_form')}
            ]
        }
        else:
            context = {
                'title': 'Daftar Transaksi',
                'admin_links': [
                {'name': 'create', 'url' : reverse('transaksi:transaksi_form')}
            ]
        }
        return render(request, 'tindakan/generic_read.html', context)

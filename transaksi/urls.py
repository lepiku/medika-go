from django.urls import include,path
from .views import form,update,read,delete
app_name='transaksi'
urlpatterns =[
    path('create/',form,name='transaksi_form'),
    path('update/<str:id>/',update,name='transaksi_update'),
    path('read/',read,name='transaksi_read'),
    path('delete/<str:id>/',delete,name='transaksi_delete')
    ]

from django import forms
from django.db import connection

class DateInput(forms.DateInput):
    input_type = 'date'

class DateTimeInput(forms.DateTimeInput):
    input_type='datetime'

class transaksiForm(forms.Form):
    no_rekam_medis=forms.ChoiceField(label='Nomor Rekam Medis Pasien:')
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT no_rekam_medis FROM PASIEN")
            numbers = cursor.fetchall()
        self.fields['no_rekam_medis'].choices = [(r[0], r[0]) for r in numbers]

class transaksiUpdateForm(forms.Form):
    id_transaksi=forms.CharField(label='ID Transaksi:',disabled=False)
    tanggal=forms.DateField(label='Tanggal:',widget=DateInput())
    status=forms.CharField(label='Status:',max_length=50)
    total_biaya=forms.IntegerField(label='Total Biaya:',disabled=False)
    waktu_pembiayaan=forms.DateTimeField(label='Waktu Pembayaran:',widget=DateTimeInput())
    no_rekam_medis_pasien=forms.CharField(label='Nomor Rekam Medis Pasien:', max_length=50,disabled=False)

from django.db import connection
from django.shortcuts import redirect, render
from django.urls import reverse

from .forms import (TindakanPoliForm, TindakanForm, TindakanPoliUpdateForm,
                    TindakanUpdateForm)
from pengguna.utils import role_required

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def dictfetchone(cursor):
    "Return one row from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return dict(zip(columns, cursor.fetchone()))


@role_required('admin')
def poli_create(request):
    if request.method == 'POST':
        form = TindakanPoliForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('tindakan:poli-read')
    else:
        form = TindakanPoliForm()

    context = {
        'form': form,
        'title': 'Form Pembuatan Tindakan Poliklinik',
        'target_url': reverse('tindakan:poli-create'),
    }

    return render(request, 'tindakan/generic_form.html', context)


def poli_read(request):
    header = [
        'ID Tindakan Poliklinik',
        'ID Poliklinik',
        'Nama Tindakan',
        'Deskripsi',
        'Tarif',
    ]

    with connection.cursor() as cursor:
        cursor.execute('''SELECT id_tindakan_poli, id_poliklinik, nama_tindakan,
                          deskripsi, tarif FROM tindakan_poli
                          ORDER BY id_tindakan_poli''')
        data = dictfetchall(cursor)

    for i, d in enumerate(data):
        data[i]['pk'] = d['id_tindakan_poli']

    context = {
        'title': 'Daftar Tindakan Poliklinik',
        'header': header,
        'data': data,
        'update_url': '/tindakan/poliklinik/update/',
        'delete_url': '/tindakan/poliklinik/delete/',
        'admin_links': [
            {'name': 'create', 'url': reverse('tindakan:poli-create')},
        ],
    }

    return render(request, 'tindakan/generic_read.html', context)


@role_required('admin')
def poli_update(request, pk):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT * FROM tindakan_poli WHERE id_tindakan_poli = %s",
            [pk,],
        )
        tindakan_poli = dictfetchone(cursor)

    if request.method == 'POST':
        form = TindakanPoliUpdateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('tindakan:poli-read')
    else:
        form = TindakanPoliUpdateForm(tindakan_poli)

    context = {
        'form': form,
        'title': 'Form Update Tindakan Poliklinik',
        'target_url': reverse('tindakan:poli-update', kwargs={'pk': pk}),
        'submit_text': 'Update',
    }

    return render(request, 'tindakan/generic_form.html', context)


@role_required('admin')
def poli_delete(request, pk):
    with connection.cursor() as cursor:
        cursor.execute(
            "DELETE FROM tindakan_poli WHERE id_tindakan_poli = %s", (pk,))
    return redirect('tindakan:poli-read')


@role_required('admin')
def create(request):
    if request.method == 'POST':
        form = TindakanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('tindakan:read')
    else:
        form = TindakanForm()

    context = {
        'form': form,
        'title': 'Form Pembuatan Tindakan',
        'target_url': reverse('tindakan:create'),
        'submit_text': 'Submit',
    }

    return render(request, 'tindakan/generic_form.html', context)


def read(request):
    header = [
        'ID Konsultasi',
        'No Urut',
        'Biaya',
        'Catatan',
        'ID Transaksi',
        'Daftar ID Tindakan Poli',
    ]

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM tindakan ORDER BY no_urut::INTEGER")
        data = dictfetchall(cursor)
        cursor.execute("SELECT * FROM daftar_tindakan")
        daftar = dictfetchall(cursor)

    grouped_data = {}
    for d in daftar:
        key = (d['id_konsultasi'], d['no_urut'])
        if key in grouped_data:
            grouped_data[key].append(d['id_tindakan_poli'])
        else:
            grouped_data[key] = [d['id_tindakan_poli'],]

    for d in data:
        key = (d['id_konsultasi'], d['no_urut'])
        if key in grouped_data:
            d['daftar_id_tindakan_poli'] = grouped_data[key]
        else:
            d['daftar_id_tindakan_poli'] = []
        d['pk'] = d['id_konsultasi'] + '/' + d['no_urut']

    context = {
        'title': 'Daftar Tindakan',
        'header': header,
        'data': data,
        'update_url': '/tindakan/update/',
        'delete_url': '/tindakan/delete/',
        'links': [
            {'name': 'poliklinik', 'url': reverse('tindakan:poli-read')},
        ],
        'admin_links': [
            {'name': 'create', 'url': reverse('tindakan:create')},
        ]
    }

    return render(request, 'tindakan/generic_read.html', context)


@role_required('admin')
def update(request, id_konsul, no_urut):
    with connection.cursor() as cursor:
        cursor.execute('''SELECT * FROM tindakan
                          WHERE id_konsultasi = %s AND no_urut = %s''',
                       [id_konsul, no_urut])
        tindakan = dictfetchone(cursor)

        cursor.execute('''SELECT id_tindakan_poli FROM daftar_tindakan
                          WHERE id_konsultasi = %s AND no_urut = %s''',
                       [id_konsul, no_urut])
        raw_daftar = cursor.fetchall()
        tindakan['daftar_tindakan_poli'] = [r[0] for r in raw_daftar]

    if request.method == 'POST':
        form = TindakanUpdateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('tindakan:read')
    else:
        form = TindakanUpdateForm(tindakan)

    context = {
        'form': form,
        'title': 'Form Update Tindakan',
        'target_url': reverse(
            'tindakan:update',
            kwargs={'id_konsul': id_konsul, 'no_urut': no_urut}),
        'submit_text': 'Update',
    }

    return render(request, 'tindakan/generic_form.html', context)


@role_required('admin')
def delete(request, id_konsul, no_urut):
    with connection.cursor() as cursor:
        cursor.execute(
            "DELETE FROM daftar_tindakan WHERE id_konsultasi = %s AND no_urut = %s",
            (id_konsul, no_urut))
        cursor.execute(
            "DELETE FROM tindakan WHERE id_konsultasi = %s AND no_urut = %s",
            (id_konsul, no_urut))
    return redirect('tindakan:read')

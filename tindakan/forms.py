from django import forms
from django.db import connection

class TindakanPoliForm(forms.Form):
    id_poliklinik = forms.ChoiceField()
    nama_tindakan = forms.CharField(max_length=50)
    deskripsi = forms.CharField(widget=forms.Textarea(attrs={'rows': 3}))
    tarif = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        with connection.cursor() as cursor:
            cursor.execute("SELECT id_poliklinik FROM layanan_poliklinik")
            rows = cursor.fetchall()

        self.fields['id_poliklinik'].choices = [(r[0], r[0]) for r in rows]

    def save(self):
        with connection.cursor() as cursor:
            cursor.execute('SELECT id_tindakan_poli FROM tindakan_poli ORDER BY id_tindakan_poli DESC')
            try:
                last_id = cursor.fetchone()[0]
            except IndexError:
                last_id = 'TP0'

            next_id = last_id[:2] + str(int(last_id[2:]) + 1)

            cursor.execute('INSERT INTO tindakan_poli VALUES(%s, %s, %s, %s, %s)',
                (
                    next_id,
                    self.cleaned_data['id_poliklinik'],
                    self.cleaned_data['nama_tindakan'],
                    self.cleaned_data['tarif'],
                    self.cleaned_data['deskripsi'],
                ))


class TindakanPoliUpdateForm(forms.Form):
    id_tindakan_poli = forms.CharField(
        label='ID Tindakan Poliklinik',
        max_length=50,
        widget=forms.TextInput({'readonly': True})
    )

    id_poliklinik = forms.ChoiceField()
    nama_tindakan = forms.CharField(max_length=50)
    deskripsi = forms.CharField(widget=forms.Textarea(attrs={'rows': 3}))
    tarif = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        with connection.cursor() as cursor:
            cursor.execute("SELECT id_poliklinik FROM layanan_poliklinik")
            rows = cursor.fetchall()

        self.fields['id_poliklinik'].choices = [(r[0], r[0]) for r in rows]
        self.fields['id_tindakan_poli'].initial = args[0]['id_tindakan_poli']

    def save(self):
        with connection.cursor() as cursor:
            cursor.execute('''UPDATE tindakan_poli SET id_poliklinik=%s,
                nama_tindakan = %s, tarif = %s, deskripsi = %s
                WHERE id_tindakan_poli = %s''',
                (
                    self.cleaned_data['id_poliklinik'],
                    self.cleaned_data['nama_tindakan'],
                    self.cleaned_data['tarif'],
                    self.cleaned_data['deskripsi'],
                    self.cleaned_data['id_tindakan_poli'],
                ))

class TindakanForm(forms.Form):
    id_konsultasi = forms.ChoiceField()
    id_transaksi = forms.ChoiceField()
    catatan = forms.CharField(widget=forms.Textarea(attrs={'rows': 3}))
    daftar_tindakan_poli = forms.MultipleChoiceField(
        label='Daftar ID Tindakan Poliklinik',
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        with connection.cursor() as cursor:
            cursor.execute("SELECT id_konsultasi FROM sesi_konsultasi")
            konsul = cursor.fetchall()
            cursor.execute("SELECT id_transaksi FROM transaksi")
            transaksi = cursor.fetchall()
            cursor.execute("SELECT id_tindakan_poli FROM tindakan_poli")
            poli = cursor.fetchall()

        self.fields['id_konsultasi'].choices = [(r[0], r[0]) for r in konsul]
        self.fields['id_transaksi'].choices = [(r[0], r[0]) for r in transaksi]
        self.fields['daftar_tindakan_poli'].choices = [(r[0], r[0]) for r in poli]

    def save(self):
        with connection.cursor() as cursor:
            cursor.execute('SELECT no_urut FROM tindakan ORDER BY no_urut::INTEGER DESC LIMIT 1')
            try:
                last_no_urut = cursor.fetchone()[0]
            except IndexError:
                last_no_urut = '0'

            next_no_urut = str(int(last_no_urut) + 1)

            cursor.execute(
                'INSERT INTO tindakan VALUES(%s, %s, %s, %s, %s)',
                (
                    self.cleaned_data['id_konsultasi'],
                    next_no_urut,
                    0,
                    self.cleaned_data['catatan'],
                    self.cleaned_data['id_transaksi'],
                ))

            for id_tindakan_poli in self.cleaned_data['daftar_tindakan_poli']:
                cursor.execute(
                    'INSERT INTO daftar_tindakan VALUES(%s, %s, %s)',
                    (
                        self.cleaned_data['id_konsultasi'],
                        next_no_urut,
                        id_tindakan_poli,
                    ))


class TindakanUpdateForm(forms.Form):
    id_konsultasi = forms.ChoiceField(widget=forms.Select({'readonly': True}))
    no_urut = forms.CharField(widget=forms.TextInput({'readonly': True}))
    biaya = forms.IntegerField(widget=forms.NumberInput({'readonly': True}))
    catatan = forms.CharField(widget=forms.Textarea(attrs={'rows': 3}))
    id_transaksi = forms.ChoiceField(widget=forms.Select({'readonly': True}))
    daftar_tindakan_poli = forms.MultipleChoiceField(
        widget=forms.SelectMultiple({'readonly': True}),
        label='Daftar ID Tindakan Poliklinik',
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        with connection.cursor() as cursor:
            cursor.execute("SELECT id_konsultasi FROM sesi_konsultasi")
            konsul = cursor.fetchall()
            cursor.execute("SELECT id_transaksi FROM transaksi")
            transaksi = cursor.fetchall()
            cursor.execute("SELECT id_tindakan_poli FROM tindakan_poli")
            poli = cursor.fetchall()

        self.fields['id_konsultasi'].choices = [(r[0], r[0]) for r in konsul]
        self.fields['id_transaksi'].choices = [(r[0], r[0]) for r in transaksi]
        self.fields['daftar_tindakan_poli'].choices = [(r[0], r[0]) for r in poli]

    def save(self):
        with connection.cursor() as cursor:
            cursor.execute(
                'UPDATE tindakan SET catatan=%s WHERE id_konsultasi=%s AND no_urut=%s',
                (
                    self.cleaned_data['catatan'],
                    self.cleaned_data['id_konsultasi'],
                    self.cleaned_data['no_urut'],
                ))

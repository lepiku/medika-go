from django.urls import path
from . import views

app_name = 'tindakan'

urlpatterns = [
    path('poliklinik/create/', views.poli_create, name='poli-create'),
    path('poliklinik/', views.poli_read, name="poli-read"),
    path('poliklinik/update/<str:pk>/', views.poli_update, name="poli-update"),
    path('poliklinik/delete/<str:pk>/', views.poli_delete, name="poli-delete"),
    path('create/', views.create, name='create'),
    path('', views.read, name="read"),
    path('update/<str:id_konsul>/<str:no_urut>/', views.update, name='update'),
    path('delete/<str:id_konsul>/<str:no_urut>/', views.delete, name='delete'),
]

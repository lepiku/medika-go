from django import forms
from django.db import connection
'''
class RegistDokterCabang(forms.Form):
    
    records_id_dokter = [
        ('D1','D1'),
        ('D2','D2'),
        ('D3','D3'),
        ('D4','D4'),
        ('D5','D5'),
        ('D6','D6'),
        ('D7','D7'),
    ]

    records_kode_rs = [
        ('RS1', 'RS1'),
        ('RS2', 'RS2'),
        ('RS3', 'RS3'),
        ('RS4', 'RS4'),
        ('RS5', 'RS5'),
        ('RS6', 'RS6'),
    ]

    judul_regist = 'FORM REGISTRASI DOKTER-RS CABANG'     
    judul_update = 'FROM UPDATE DOKTER-RS CABANG'           
    id_dokter = forms.CharField(widget=forms.Select(choices=records_id_dokter))
    kode_rs = forms.CharField(widget=forms.Select(choices=records_kode_rs))

    def save(self):
        pass
'''
class RegistDokterCabang(forms.Form):
    id_dokter=forms.ChoiceField(label='ID Dokter:')
    kode_rs=forms.ChoiceField(label='Kode RS Cabang:')
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT kode_rs FROM dokter_rs_cabang ORDER BY kode_rs ASC")
            hospitals = cursor.fetchall()
            cursor.execute("SELECT id_dokter FROM dokter_rs_cabang ORDER BY id_dokter ASC")
            doctors = cursor.fetchall()
        self.fields['id_dokter'].choices = [(r[0], r[0]) for r in doctors]
        self.fields['kode_rs'].choices = [(r[0], r[0]) for r in hospitals]
    
    def save(self):
        pass
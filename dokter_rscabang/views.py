from django.shortcuts import render, redirect, HttpResponse
from django.http import HttpResponseRedirect
from .forms import RegistDokterCabang
from django.db import connection, IntegrityError
from django.urls import reverse
from django.http import HttpResponse,JsonResponse
from tindakan.views import dictfetchall,dictfetchone
from pengguna.utils import role_required

# Create your views here.
@role_required("admin")
def create(request):
    form=RegistDokterCabang()
    if(request.method=="POST"):
        form=RegistDokterCabang(request.POST)
        if(form.is_valid()):
            kode_rs=form.cleaned_data['kode_rs']
            id_dokter=form.cleaned_data['id_dokter']
            try:
                with connection.cursor() as cursor1:
                    cursor1.execute("INSERT INTO dokter_rs_cabang VALUES (%s,%s)",[id_dokter,kode_rs])
                return redirect('dokter_rscabang:daftar')
            except IntegrityError:
                form.add_error('id_dokter','Maaf data yang dimasukkan sudah tersedia')
                form.add_error('kode_rs','Maaf data yang dimasukkan sudah tersedia')
                context={
                    'form': form,
                    'title': 'Form Pendaftaran Dokter RS Cabang',
                    'target_url' : reverse('dokter_rscabang:register')
                }     
                return render(request,'tindakan/generic_form.html',context)

    context={
            'form': form,
            'title': 'Form Pendaftaran Dokter RS Cabang',
            'target_url' : reverse('dokter_rscabang:register')
        }     
    return render(request,'tindakan/generic_form.html',context)

@role_required("admin")
def update(request,id):
    pk=id.split('-')
    kode_rs=pk[0]
    id_dokter=pk[1]    
    selected_doc=''
    with connection.cursor() as cursor1:
        cursor1.execute("SELECT FROM dokter_rs_cabang WHERE kode_rs= %s AND  id_dokter= %s",[kode_rs,id_dokter])
        selected_doc=dictfetchone(cursor1)
    if(request.method=='POST'):
        form=RegistDokterCabang(request.POST)
        if(form.is_valid()):
            kode_rs=form.cleaned_data['kode_rs']
            id_dokter=form.cleaned_data['id_dokter']
            with connection.cursor() as cursor1:
                cursor1.execute("UPDATE dokter_rs_cabang SET kode_rs=%s WHERE id_dokter=%s",[kode_rs,id_dokter])
            return redirect('dokter_rscabang:daftar')
        return HttpResponse(status=404)
    else:
        form=RegistDokterCabang()
        form.fields['id_dokter'].label='{}{} '.format(form.fields['id_dokter'].label,id_dokter)
        form.fields['kode_rs'].label='{}{} '.format(form.fields['kode_rs'].label,kode_rs)
        context={
            'form': form,
            'title': 'Form Update Dokter RS Cabang',
            'target_url' : reverse('dokter_rscabang:update-dokter', kwargs={'id': id}),
            'submit_text':'Update'
        }
        return render(request,'tindakan/generic_form.html',context)

@role_required("admin")
def delete(request,id):
    try:
        pk=id.split('-')
        kode_rs=pk[0]
        id_dokter=pk[1]
        with connection.cursor() as cursor1:
            cursor1.execute("DELETE FROM dokter_rs_cabang WHERE dokter_rs_cabang.kode_rs= %s AND  dokter_rs_cabang.id_dokter= %s",[kode_rs,id_dokter])
        return redirect('dokter_rscabang:daftar')
    except IntegrityError:
        return HttpResponse("ERROR: Primary key is still being referenced!")

def read(request):
    limit=5
    if(request.session.is_empty()):
        return redirect("pengguna:profile")
    elif (request.method=="POST"):
        mode=request.POST.get("mode","forward")
        if(mode=="forward"):
            offset=limit*int(request.POST.get("offset",0))
        else:
            offset=limit*(int(request.POST.get("offset",0))-2)
        data=dict()
        with connection.cursor() as cursor2:
            cursor2.execute("SELECT kode_rs,STRING_AGG(id_dokter, ',') AS id_dokter FROM dokter_rs_cabang GROUP BY kode_rs ORDER BY kode_rs ASC OFFSET %s LIMIT %s",[offset,limit])
            data=dictfetchall(cursor2)
        for i, d in enumerate(data):
            data[i]['pk'] = ['{}-{}'.format(d['kode_rs'],doc_id) for doc_id in d['id_dokter'].split(',')]
            data[i]['id_dokter']=d['id_dokter'].split(',')
        return JsonResponse(data,safe=False)

    else:
        header = [
            'No.',
            'Kode RS Cabang',
            'ID Dokter'
        ]
        data=[]
        selected_data=[]
        with connection.cursor() as cursor:
            cursor.execute("SELECT kode_rs,STRING_AGG(id_dokter, ',') AS id_dokter FROM dokter_rs_cabang GROUP BY kode_rs ORDER BY kode_rs ASC")
            data = dictfetchall(cursor)
        
        with connection.cursor() as cursor:
            cursor.execute("SELECT kode_rs,STRING_AGG(id_dokter, ',') AS id_dokter FROM dokter_rs_cabang GROUP BY kode_rs ORDER BY kode_rs ASC  LIMIT %s",[limit])
            selected_data = dictfetchall(cursor)

        if(len(selected_data) < len(data)):
            data=selected_data
            pagination=True

        for i, d in enumerate(data):
            data[i]['pk'] = ['{}-{}'.format(d['kode_rs'],doc_id) for doc_id in d['id_dokter'].split(',')]
            data[i]['id_dokter']=d['id_dokter'].split(',')

        context = {
            'header': header,
            'data': data,
            'update_url': '/dokter/updatedokter/',
            'delete_url': '/dokter/deletedokter/',
            'api_url':'/dokter/daftardokter/',
            'admin_links': [
            {'name': 'create', 'url' : reverse('dokter_rscabang:register')}
            ],
            'pagination':pagination
        }
        return render(request, 'dokter_rs_read.html', context)


from django.urls import path
from .views import read,delete,create,update

app_name = 'dokter_rscabang'

urlpatterns = [
    path('register/', create, name='register'),
    path('daftardokter/', read, name='daftar'),
    path('deletedokter/<str:id>/',delete,name='delete-dokter'),
    path('updatedokter/<str:id>/',update,name='update-dokter')
]

from django.apps import AppConfig


class DokterRscabangConfig(AppConfig):
    name = 'dokter_rscabang'

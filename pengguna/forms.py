from django import forms
from django.db import connection


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class RegisterPenggunaForm(LoginForm):
    nomor_identitas = forms.CharField()
    nama_lengkap = forms.CharField()
    tanggal_lahir = forms.DateField()
    email = forms.EmailField()
    alamat = forms.CharField(widget=forms.Textarea(attrs={'rows': 3}))

    def save(self):
        with connection.cursor() as cursor:
            cursor.execute(
                'INSERT INTO pengguna VALUES (%s, %s, %s, %s, %s, %s, %s)',
                (
                    self.cleaned_data['email'],
                    self.cleaned_data['username'],
                    self.cleaned_data['password'],
                    self.cleaned_data['nama_lengkap'],
                    self.cleaned_data['nomor_identitas'],
                    self.cleaned_data['tanggal_lahir'],
                    self.cleaned_data['alamat'],
                ))

class RegisterPasienForm(RegisterPenggunaForm):
    alergi = forms.MultipleChoiceField(required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # disable option validation for alergi
        if len(args) >= 1:
            print([(x, x) for x in args[0].getlist('alergi')])
            self.fields['alergi'].choices = [(x, x) for x in args[0].getlist('alergi')]

    def save(self):
        super().save()

        with connection.cursor() as cursor:
            # get last asuransi
            cursor.execute('SELECT nama FROM asuransi LIMIT 1')
            asuransi = cursor.fetchone()[0]

            # get last pasien id
            cursor.execute(
                'SELECT no_rekam_medis FROM pasien ORDER BY no_rekam_medis DESC LIMIT 1')
            try:
                prev_id = cursor.fetchone()[0]
            except IndexError:
                prev_id = '10000000'
            next_id = str(int(prev_id) + 1)

            # store pasien
            cursor.execute(
                'INSERT INTO pasien VALUES (%s, %s, %s)',
                (
                    next_id,
                    self.cleaned_data['username'],
                    asuransi,
                ))

            # store alergi
            for nama_alergi in self.cleaned_data['alergi']:
                cursor.execute(
                    'INSERT INTO alergi_pasien VALUES (%s, %s)',
                    (next_id, nama_alergi))


class RegisterDokterForm(RegisterPenggunaForm):
    no_sip = forms.CharField()
    spesialisasi = forms.CharField()

    def save(self):
        super().save()
        with connection.cursor() as cursor:
            cursor.execute('SELECT id_dokter FROM dokter ORDER BY id_dokter DESC LIMIT 1')
            try:
                prev_id = cursor.fetchone()[0]
            except IndexError:
                prev_id = 'D0'
            next_id = prev_id[:1] + str(int(prev_id[1:]) + 1)

            cursor.execute(
                'INSERT INTO dokter VALUES (%s, %s, %s, %s)',
                (
                    next_id,
                    self.cleaned_data['username'],
                    self.cleaned_data['no_sip'],
                    self.cleaned_data['spesialisasi'],
                ))

from django.shortcuts import redirect, render, reverse
from django.http import HttpResponseNotFound
#from django.contrib.auth import (authenticate, login as django_login,logout as django_logout)
from .forms import LoginForm, RegisterPasienForm, RegisterDokterForm
from django.db import connection
from django.core.serializers.json import DjangoJSONEncoder
import json

register_forms = {
    'pasien': RegisterPasienForm,
    'dokter': RegisterDokterForm,
}


def register_redirect(request):
    if request.session.is_empty():
        return redirect('pengguna:profile')
    else:
        if(request.session['success_registered']):
            context={'role':request.session['role']}
            request.session['success_registered']=False
            return render(request,'redirect_success.html',context)
        else:
            return redirect('pengguna:profile')


def general_profile(request):
    if 'username' not in request.session:
        return render(request,'index.html')
    elif request.session['role'] == 'admin':
        return redirect('admin_rs:admin_profile')

    context = {
        'data': {
            'Username': request.session['username'],
            'Nomor Identitas': request.session['nomor_id'],
            'Nama Lengkap': request.session['nama_lengkap'],
            'Tanggal Lahir': request.session['tanggal_lahir'],
            'Email': request.session['email'],
            'Alamat': request.session['alamat'],
        }
    }
    if request.session.get('role') == 'pasien':
        with connection.cursor() as cursor:
            cursor.execute(
                'SELECT no_rekam_medis FROM pasien WHERE username = %s',
                (request.session['username'],))
            no_rekam_medis = cursor.fetchone()[0]

            cursor.execute(
                'SELECT alergi FROM alergi_pasien WHERE no_rekam_medis = %s',
                (no_rekam_medis,))

            alergi = [x[0] for x in cursor.fetchall()]

        context['data'].update({
            'No. Rekam Medis': no_rekam_medis,
            'Alergi': alergi,
        })

    if request.session.get('role') == 'dokter':
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM dokter WHERE username = %s',
                           (request.session['username'],))
            dokter = cursor.fetchone()
        context['data'].update({
            'No. SIP': dokter[2],
            'Spesialisasi': dokter[3],
            })

    return render(request, 'pengguna/profile.html',context)

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            if auth_login(request,form.cleaned_data['username'], form.cleaned_data['password']):
                return redirect('pengguna:profile')
            else:
                form.add_error(None, 'Wrong username or password')
    else:
        form = LoginForm()

    context = {
        'form': form,
        'title': 'Form Login',
        'submit_text': 'Login',
        'links': [{'name': 'register', 'url': reverse('pengguna:register')}],
    }
    return render(request, 'login_form.html', context)


def register(request):
    return render(request, 'pengguna/register.html')


def register_role(request, role):
    if role in register_forms:
        FormClass = register_forms[role]
    else:
        return HttpResponseNotFound('<h1>Role not found</h1>')

    if request.method == 'POST':
        form = FormClass(request.POST)
        if form.is_valid():
            form.save()
            if auth_login(request, form.cleaned_data['username'],
                          form.cleaned_data['password']):
                return redirect('pengguna:profile')
            form.add_error(None, 'Cannot login.')

    else:
        form = FormClass()

    context = {
        'form': form,
        'title': 'Form Pendaftaran %s' % role.title(),
        'target_url': reverse('pengguna:register-role', kwargs={'role': role}),
        'submit_text': 'Daftar',
    }

    if role == 'pasien':
        return render(request, 'pengguna/register_pasien.html', context)
    return render(request, 'tindakan/generic_form.html', context)


def logout(request):
    request.session.flush()
    return redirect('/')


def auth_login(request,username,password):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM PENGGUNA WHERE username=%s AND password=%s',[username,password])
        user=cursor.fetchone()
        if (user != None):
            request.session['email']=user[0]
            request.session['username']=user[1]
            request.session['password']=user[2]
            request.session['nama_lengkap']=user[3]
            request.session['nomor_id']=user[4]
            request.session['tanggal_lahir']=json.dumps(user[5], cls=DjangoJSONEncoder)[1:-1]
            request.session['alamat']=user[6]
            request.session['role']=get_role(username)
            if(request.session['role']=='admin'):
                cursor.execute('SELECT  kode_rs FROM ADMINISTRATOR WHERE username=%s',[username])
                admin=cursor.fetchone()
                request.session['kode_rs']=admin[0]
            elif(request.session['role']=='dokter'):
                cursor.execute('SELECT  id_dokter FROM DOKTER WHERE username=%s',[username])
                dokutah=cursor.fetchone()
                request.session['id_dokter']=dokutah[0]
            else:
                cursor.execute('SELECT  no_rekam_medis FROM PASIEN WHERE username=%s',[username])
                pasien=cursor.fetchone()
                request.session['no_rekam_medis']=pasien[0]
            return True
        else:
            return False

def get_role(username):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM ADMINISTRATOR WHERE username = %s", [username])
        if (cursor.fetchone() != None):
            return "admin"
        cursor.execute("SELECT * FROM DOKTER WHERE username = %s", [username])
        if (cursor.fetchone() != None):
            return "dokter"
        return "pasien"

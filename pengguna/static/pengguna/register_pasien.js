$(document).ready(function () {
  $("#add-alergi").keypress(function (e) {
    if (e.which == 13) {
      let value = $(this).val();

      console.log(value);
      $("select#id_alergi").append(
        `<option selected value=${value}>${value}</option>`
      );

      $(this).val("");

      return false;
    }
  });

  $("button#add-typed-alergi").click(function () {
    let e = $.Event("keypress");
    e.which = 13;
    e.keyCode = 13; //keycode to trigger this for simulating enter
    $("#add-alergi").trigger(e);
    return false;
  });
});

from django.urls import path
from . import views

app_name = 'pengguna'

urlpatterns = [
    path('', views.general_profile, name='profile'),
    path('redirect/',views.register_redirect,name='redirect'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('register/<str:role>/', views.register_role, name='register-role'),
    path('logout/', views.logout, name='logout'),
]

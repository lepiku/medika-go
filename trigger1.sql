/*the function*/
CREATE OR REPLACE FUNCTION hitung_biaya_konsul()
    RETURNS trigger AS 
$$
BEGIN
    IF (NEW.tanggal=OLD.tanggal) THEN
        UPDATE  sesi_konsultasi SET biaya=95000 WHERE id_konsultasi=OLD.id_konsultasi;
        RETURN NEW;
    ELSE
         UPDATE  sesi_konsultasi SET biaya=125000*ABS(NEW.tanggal-OLD.tanggal) WHERE id_konsultasi=OLD.id_konsultasi;
         RETURN NEW;
    END IF;
END;
$$ LANGUAGE plpgsql;

/*trigger*/
CREATE TRIGGER status_to_done AFTER UPDATE OF  STATUS ON sesi_konsultasi 
FOR EACH ROW 
WHEN (NEW.status LIKE '%Done%')
EXECUTE PROCEDURE hitung_biaya_konsul();

